import React, { Fragment, useState, useEffect } from "react";
import Header from "./components/Header";
import Formulario from "./components/Formulario";
import Clima from "./components/Clima";
import Error from "./components/Error";
function App() {
  const [busqueda, guardarBusqueda] = useState({
    ciudad: "",
    pais: "",
  });
  const { ciudad, pais } = busqueda;
  const [consultar, guardarConsultar] = useState(false);
  const [resultado, guardarResultado] = useState({});
  const [error, guardarError]=useState(false)
  useEffect(() => {
    const consultarAPI = async () => {
      if (consultar) {
        const appId = "9af266042f9af417d61a6fba027a4cf0";
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}
        `;
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        guardarResultado(resultado)
        guardarConsultar(false)
        if(resultado.cod === "404"){
          guardarError(true)
        }else{
          guardarError(false)
        }

      }
    };
    consultarAPI();
  }, [consultar]);
  let componente;
  if(error){
    componente= <Error mensaje="No se encontró la ciudad" ></Error>
  }else{
    componente=<Clima resultado={resultado}></Clima>
  }
  return (
    <Fragment>
      <Header titulo="Consulta el Clima"></Header>
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario
                busqueda={busqueda}
                guardarBusqueda={guardarBusqueda}
                guardarConsultar={guardarConsultar}
              ></Formulario>
            </div>
            <div className="col m6 s12">
              {componente}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
