import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Formulario = ({busqueda, guardarBusqueda, guardarConsultar}) => {

    

    const [error, guardarError]=useState(false)

    //extraer ciudad y pais
    const {ciudad, pais}= busqueda;



    const handleSubmit = e =>{
        e.preventDefault();
        if(ciudad.trim()===''|| pais.trim()===''){
            guardarError(true);
            return
        }
        guardarError(false)
        guardarConsultar(true);
        
    }
    
    
    //funcion que coloca los elementos en el state
    const handleChange = e=>{
        guardarBusqueda({
            ...busqueda,
            [e.target.name]: e.target.value

        })
    }

    
    return (
        <form
        onSubmit={handleSubmit}>
            {error ? <p className="red darken-4 error">Todos los campos son obligatorios</p>: null}
            <div className='input-field col s12'> 
                <input
                type='text'
                name='ciudad'
                id='ciudad'
                value={ciudad}
                onChange={handleChange}>
                    
                </input>
                <label htmlFor='ciudad'>Ciudad:</label>
            </div>
            <div className="input-field col s12">
                <select 
                name='pais' 
                id='pais'
                value={pais}
                onChange={handleChange}>
                    <option value=""> --- Seleccione un País---</option>
                    <option value="US">Estados Unidos</option>
                    <option value="MX">México</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">España</option>
                    <option value="PE">Perú</option>
                    <option value="EC">Ecuador</option>
                </select>
                <label htmlFor='pais'>País:</label>

            </div>
            <div className="input-field col s12">
                <input
                 type='submit'
                 value= 'Buscar Clima'
                 className="waves-effect waves-light btn-large btn-block yellow accent-4">
                </input>

            </div>


        </form>
    );
}

Formulario.propTypes={
    busqueda:PropTypes.object.isRequired,
    guardarBusqueda:PropTypes.func.isRequired,
    guardarConsultar:PropTypes.func.isRequired
    
}
 
export default Formulario;
